﻿using App2.Services;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.NetworkInformation;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Threading;
using System.Threading.Tasks;
using Utf8Json;
using Xamarin.Forms;

namespace Nekorhite
{
    public partial class MainPage : ContentPage
    {

        private BackgroundService Service => BackgroundService.Instance;
        //private readonly JsonSerializerOptions options = new JsonSerializerOptions { PropertyNameCaseInsensitive = true };

        public MainPage()
        {
            InitializeComponent();
            Initialize();
            OnNavigated();
        }

        private void OnNavigated()
        {
            new Animation()
                .WithConcurrent(x => Gradient1.Offset = (float)x, 0.0, 1.0)
                .WithConcurrent(x => Gradient2.Offset = (float)x, 0.1, 1.1)
                .WithConcurrent(x => Gradient3.Offset = (float)x, 0.2, 1.2)
                .Commit(this, "gradient1", 16, 2000, repeat: () => true);
            webview.Navigated  /**/ += (x, y) => new Animation(z => webview.Opacity = z, 0, 1).Commit(this, "webin");
        }

        private async void Initialize()
        {
            var state = State.Waiting;
            Service.Initialize();
            await Task.Delay(1000);
            while (true)
            {
                await Task.Delay(1000);
                if (Service.IsExists && state == State.Waiting)
                {
                    new Animation(z => splash.Opacity = z, 1, 0).Commit(this, "splashout");
                    state = State.Active;
                    webview.Source = $"http://{Service.ServerAddress}";
                }
                if (!Service.IsExists && state == State.Active)
                {
                    new Animation(z => splash.Opacity = z, 0, 1).Commit(this, "splashin");
                    await Task.Delay(500);
                    new Animation(z => webview.Opacity = z, 1, 0).Commit(this, "webout");
                    await Task.Delay(500);
                    webview.Source = $"http://{"localhost"}/";
                    state = State.Waiting;
                }
            }
        }

        public class Network
        {
            [DataMember(Name = "addressList")]
            public IEnumerable<string> AddressList { get; set; }
        }

        private enum State { Waiting, Active }
    }
}
