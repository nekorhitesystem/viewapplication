﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Net;
using System.Net.Http;
using System.Net.Sockets;
using System.Text;
using System.Threading.Tasks;

namespace App2.Services
{
    internal class BackgroundService : INotifyPropertyChanged
    {
        public event PropertyChangedEventHandler PropertyChanged;

        public static BackgroundService Instance { get; } = new BackgroundService();

        private readonly HttpClient client = new HttpClient() { Timeout = TimeSpan.FromSeconds(5) };

        public string ServerAddress { get; private set; }

        public bool IsExists { get; private set; }

        private BackgroundService()
        {
            
        }

        internal void Initialize()
        {
            _ = Task.Run(FindServerAsync);
        }

        private async Task FindServerAsync()
        {
            while(true)
            {
                try
                {
                    using (var udpclient = new UdpClient(new IPEndPoint(IPAddress.Any, 65412)))
                    {
                        udpclient.EnableBroadcast = true;
                        udpclient.MulticastLoopback = false;
                        await udpclient.SendAsync(new byte[1] { 1 }, 1, new IPEndPoint(IPAddress.Broadcast, 65411));
                        _ = WaitServerAsync(udpclient);
                        await Task.Delay(1000);
                        client.CancelPendingRequests();
                    }
                }
                catch
                {
                    Debug.WriteLine("Task. Canceled");
                }
            }
        }

        private async Task WaitServerAsync(UdpClient udp)
        {
            try
            {
                var result = await udp.ReceiveAsync();
                var address = result.RemoteEndPoint.Address.ToString();
                using (var res = await client.GetAsync($"http://{address}/api/locale"))
                {
                    if (res.StatusCode == HttpStatusCode.OK)
                    {
                        ServerAddress = address;
                        return;
                    }
                }
            }
            catch
            {
                Debug.WriteLine("Task. Canceled");
            }
        }
    }
}
